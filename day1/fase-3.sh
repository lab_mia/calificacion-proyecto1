# Este archivo de entrada verifica casi todas las utilidades
# del proyecto, dejando pendientes los comandos cp, ren, mv
# los cuales son evaluados en otro archivo de calificacion. Este archivo
# tambien se utiliza para validar el primer y segundo nivel de indireccion.
# El tercer nivel de indireccion queda pendiente junto a las utilidades antes 
# nombradas.

mount -path="/tmp/discos calificacion/disco oficial.dk"  -name=IU #001a
login -usr=renato -pwd=201709244 -id=001a

cat -file1=/etc/nginx/nginx.conf
pause
cat -file1=/etc/pam.d/postgresql
rm -path=/etc/logrotate.d/chrony
rm -path=/etc/logrotate.d/samba
rm -path=/etc/java/security
rm -path=/etc/logrotate.d/firewalld
rm -path=/etc/logrotate.d/dnf
rep -name=ls -ruta=/etc/logrotate.d -path=/tmp/mia/reportes/updated-logrotate.png
rep -name=ls -ruta=/etc/java -path=/tmp/mia/reportes/updated-java.png

edit -path=/etc/pam.d/postgresql -stdin # Use weakref.rb as input file 1lvl of indirection
edit -cont=/tmp/RVersion.h -path=/etc/pam.d/sshd
rm -path=/etc/rc.d/init.d/vmware
touch -cont=/tmp/data/un.rb -path=/etc/rc.d/init.d/vmware #2 lvls of indirection
cat -file1=/etc/pam.d/postgresql -file2=/etc/pam.d/sshd -file3=/etc/php.d/40-zip.ini -file4=/etc/profile.d/gawk.sh -file5=/etc/rc.d/init.d/vmware
find -path=/etc -name=java*
find -path=/etc/rc.d -name=init.d?
find -path=/etc -name=*.d

rep -name=tree -root=120 -path=/tmp/mia/reportes/primer-nivel.png -id=001a #You may change 120 by the index of the /etc/pam.d directory
rep -name=tree -root=80 -path=/tmp/mia/reportes/segundo-nivel.png -id=001a #You may change 80 by the index of the /etc/rc.d/init.d directory
