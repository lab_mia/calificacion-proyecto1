mount -path=/tmp/mia/disco1.dk -name=V #001a
login -usr=root -pwd=123 -id=001a

mkdir -path=/usr
mkdir -path=/usr/include
mkdir -path=/usr/include/gnu
mkdir -path=/usr/include/qt5
touch -stdin -path=/usr/include/gnu/lib-names.h
touch -stdin -path=/usr/include/gnu/stubs-64.h
touch -stdin -path=/usr/include/gnu/stubs.h
mkdir -path=/usr/include/qt5/QtDBus
mkdir -path=/usr/include/qt5/QtDBus/5.12.5
mkdir -path=/usr/include/qt5/QtDBus/5.12.5/QtDBus
touch -stdin -path=/usr/include/qt5/QtDBus/QDBusError
touch -stdin -path=/usr/include/qt5/QtDBus/QDBusReply
touch -stdin -path=/usr/include/qt5/QtDBus/QDBusServer
touch -stdin -path=/usr/include/qt5/QtDBus/QtDBus

rep -name=tree -path=/tmp/mia/reportes/second-tree.png -id=001a
cp -path=/usr/include/gnu/lib-names.h -dest=/usr/include/gnu/stdlib.c
cp -path=/usr/include/qt5/QtDBus -dest=/usr/include/qt5/Copia
ren -path=/usr/include/gnu/stubs-64.h -name=estructuras.h
mv -path=/usr/include/qt5/QtDBus -dest=/usr
touch -cont=/tmp/data/git-log.html -path=/usr/include/gnu/git-log.html
rep -name=inode -path=/tmp/mia/reportes/V-inodes.png -id=001a
rep -name=tree -path=/tmp/mia/reportes/V-tree.png  -id=001a

rep -name=tree -root=20 -path=/tmp/mia/reportes/tercer-nivel-indireccion.png #Reemplazar 20 con el indice del inodo de git-log.html
