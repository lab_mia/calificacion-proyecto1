export EJECUTABLE=/home/alumno/muestra #Cambiar por la ruta al ejecutable de sus proyecto.
export CARNET=00 #Cambiar a los últimos dos digitos de sus carnet

sed -i "s/00/$CARNET/g" ./*.sh #No modificar
cp -r ../data /tmp #No modificar

# Si no tienes la syntaxis siguientes,  descomenta las siguientes lineas segun aplique
#
#
## Comentarios
# sed -i 's/#.\+//g' ./*.sh
#
## Espacios
# zsh remove-espacios.zsh
#
## Guiones en el nombre de los reportes
#
# sed -i 's/\([a-zAZ0-9]\+\)-\([a-zA-Z0-9]\+\)/\1_\2/g' ./*.sh
