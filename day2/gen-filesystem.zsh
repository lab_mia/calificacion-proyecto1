J=0
SG=0
V=0
function load_files(){
  cd $1
  BUFF=/tmp/buffer.sh
  for i in ./**/*; do
	  if [[ $(echo $(basename $i) | wc -m) -le 12 ]]; then
	    FULLPATH=$(readlink -f $i)
	    JM=''
	    DIRS=(${(s:/:)FULLPATH})
	    SKIP=0
	    for DIR in ${DIRS[@]}; do
		    [[ $(echo $DIR | wc -m) -gt 12 ]] && SKIP=1
		    # JK=${DIR:0:12}
		    # JM="$JM/$JK"
	    done
	    [[ $SKIP -gt 0 ]] && continue ||:
	    J=$((J+1))
	    [[ -d $i ]] && V=$((V+1)) || SG=$((SG+1))
	    [[ -d $i ]] && echo mkdir -p -path=$FULLPATH >> $BUFF || echo touch -stdin -r -path=$FULLPATH >> $BUFF
	  #[[ $J -gt 550 ]] && exit 0 ||:
	  fi
  done
}
load_files /usr/include/gnu
load_files /usr/include/qt5

echo Files written: $SG, Directories written: $V, total: $J
