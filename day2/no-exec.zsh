#!/bin/env zsh
source ./env.zsh
alias mkdisk="$EJECUTABLE mkdisk"
alias rmdisk="$EJECUTABLE rmdisk"
alias fdisk="$EJECUTABLE fdisk"
#Si no tienes Zsh instalado en tu sistema (Mac incluye zsh por default)
#deberás instalar Zsh manualmente en ese caso.
#Cada comando fdisk va acompañado de un comentario que indica como debe ir
#quedando el disco. P significa primaria, F Free, E extendida,
#L logica, G libre dentro de una extendida.
#Este archivo tiene algunos errores lógicos. Los errores están acompañados de un comentario que indica porqué debería
#ocurrir el error. En la calificación se validarán algunos escenarios extra. Este archivo debe correr y ejecutarse sin 
#ningún problema, ya que cada comando es atómico. Si ocurre un error, simplemente se detiene la ejecución y no produce
#nada el comando que lanzó el error, pero el resto de comandos deben ejecutarse sin problema. Este archivo NO se ejecuta
#con exec. El archivo de entrada para exec está separado.

OFICIAL="/tmp/discos calificacion/disco oficial.dk"
T1=/tmp/mia/disco1.dk
T2=/tmp/mia/disco2.dk
T3=/tmp/mia/disco3.dk

mkdisk -path=$T1 -size=60 -u=m -f=WF # (60000 F)
mkdisk -path=$T3 -size=208 -u=m -f=FF # ( 208000 F)
mkdisk -size=40 -u=m -f=BF -path="$OFICIAL" #(60000 F)
mkdisk -f=FF -size=55 -u=k -path=$T2 # (55 F )
rmdisk -path=$T2
fdisk -path="$OFICIAL" -add=-35 -u=K -name=p1 #Error, p1 no existe.
fdisk -path="$OFICIAL" -size=3000 -u=K -type=P -name=p1 #(3000 P; 57000 F)

fdisk -size=3000 -path="$OFICIAL" -u=K -name=p2 #(3000P; 3000P; 54000 F)
fdisk -size=25000 -path="$OFICIAL" -f=BF -u=K -type=E -name=e1 #(3000P; 3000P; 25000P; 29000F)
fdisk -size=1 -path="$OFICIAL" -u=m -type=E -name=e3  # Error, solo una particion extendida
fdisk -size=1 -path="$OFICIAL" -u=m -type=P -name=p3  # (3000P; 3000P; 25000E; 1000P; 28000F)
fdisk -delete=full -path="$OFICIAL" -name=p1  # (3000F; 3000P; 25000E; 1000P; 28000F)
fdisk -delete=full -path="$OFICIAL" -name=p2  # (6000F;  25000E; 1000P; 28000F)
fdisk -size=4500 -path="$OFICIAL" -u=k -type=P -name=n1 # (4500P; 1500F;  25000E; 1000P; 28000F)
fdisk -add=1000 -path="$OFICIAL" -u=k -name=e1   # (4500P; 500F;  26000E; 1000P; 28000F)
fdisk -delete=fast -path="$OFICIAL" -name=p3   # (4500P; 500F;  26000E; 29000F)
fdisk -add=24 -path="$OFICIAL" -u=m -name=e1 # (4500P(n1); 500F;  50000E(e1); 5000F)
  
fdisk -path="$T1" -size=5000 -u=K -type=P -name=p1 #(5000P; 55000F)
fdisk -size=10000 -path="$T1" -u=K -name=p2 #(5000P; 10000P; 45000F)
fdisk -size=15 -path="$T1" -u=M -name=p3 -type=P #(5000P; 10000P; 15000P; 30000F)
fdisk -delete=fast -path="$T1" -name=p2 #(5000P; 10000F; 15000P; 30000F)
fdisk -size=15 -path="$T1" -u=M -f=FF -name=e1 -type=E #(5000P; 10000F; 15000P; 15000E; 15000F)
fdisk -add=-1500 -u=K -name=p1 -path="$T1" #(3500P; 11500F; 15000P; 15000E; 15000F)
fdisk -size=11 -u=M -path="$T1" -type=P -name="particion 4" #(3500P; 11500F; 15000P; 15000E; 11000P; 4000F)
fdisk -add=3000 -u=K -name="particion 4" -path="$T1" #(3500P(p1); 11500F; 15000P(p3); 15000E(e1); 14000P(particion 4); 1000F)
fdisk -size=1000 -u=k -path="$T1" -type=P -name="extra"  #Error; Ya esta lleno
rmdisk -path=$T3

function fill_logicas(){
  I=50
  S=0
  while [[ $S -le $2 ]]; do
    S=$((I+S))
    fdisk -size="$I" -path="$1" -u=K -name="L$I" -type=L
    I=$((I+$3))
  done
}


fill_logicas "$OFICIAL" 9000 21 30000 


fill_logicas "$T1" 5000 30 14000  

fdisk -delete=fast -path="$OFICIAL" -name="L50" 
fdisk -delete=full -path="$OFICIAL" -name="L239" 
fdisk -delete=fast -path="$OFICIAL" -name="L575"  
fdisk -size=200 -path="$OFICIAL" -u=K -name="Nueva 200" -type=L 
fdisk -size=40 -path="$OFICIAL" -u=K -name="Nueva 40" -type=L  
fdisk -size=20 -path="$OFICIAL" -u=m -name=IU -type=L  

fdisk -size=8000 -path="$T1" -u=K -name=V -type=L  

echo Felicidades, eso es todo! Estados finales:

echo "Disco OFICIAL=/tmp/discos calificacion/disco oficial.dk; Extended size: 50000KB; Total disk size: 35MB; Partition table:"
echo "(4500(n1); 500F; [40L; 10G; 71L; 92L; 113L; 134L; 155L; 176L; 197L; 218L; 200L; 39G; 260L; 281L; 302L; 323L; 344L; 365L; 386L; 407L; 428L; 449L; 470L; 491L; 512L; 533L; 554L; 575G; 596L; 617L; 20000L; 662G]E(e1); 5000F)"

echo 'Disco $T1=/tmp/mia/disco1.dk; Extended partition: 15000KB; Total disk size: 50MB; Partition table:'
echo '(3500P(p1); 11500F; 15000P(p3); [50L; 80L; 110L; 140L; 170L; 200L; 230L; 260L; 290L; 320L; 350L; 380L; 410L; 440L; 470L; 500L; 530L; 560L; 8000L; 510G]E(e1); 14000P(particion 4); 1000F)'
