mount -path="/tmp/discos calificacion/disco oficial.dk"  -name=IU #001a
login -usr=root -pwd=123 -id=001a
chgrp -usr=liz -grp=root
chgrp -usr=alan -grp=auxiliar
chown -path=/usr/bin -r -usr=liz
chown -r -path=/etc/asciidoc -usr=alan
chmod -ugo=440 -path=/etc/asciidoc/latex.conf
chmod -ugo=774 -R -path=/usr/bin
rep -name=ls -ruta=/etc/asciidoc -path=/tmp/mia/reportes/permisos.png -id=001a
rep -name=ls -ruta=/usr/bin -path=/tmp/mia/reportes/permisos-dos.png -id=001a
logout

login -usr=renato -pwd=201709244 -id=001a
cat -file1=/etc/asciidoc/latex.conf #Error, no tengo permiso de lectura.
cat -file1=/usr/bin/ksh93 #Si funciona, ya que tengo permiso de lectura
touch -cont=/tmp/data/ruby.h -path=/usr/bin/ruby #Si funciona, ya que pertenezco al grupo de liz (root)
logout

login -usr=alan -pwd=201709946 -id=001a
touch -cont=/tmp/data/stdio.h -path=/usr/bin/zsh #NO funciona, no pertenezco al grupo y otros tiene permiso de solo lectura
edit -cont=/tmp/data/sql.h -path=/usr/asciidoc/latex.conf #NO funciona, no tiene permiso de escritura 
cat -file1=/usr/asciidoc/latex.conf #Deberia mostrar el archivo original ya que no se debio haber modificado.
logout
