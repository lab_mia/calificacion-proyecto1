#!/bin/env zsh
source ./env.zsh
alias mkdisk="$EJECUTABLE mkdisk"
alias rmdisk="$EJECUTABLE rmdisk"
alias fdisk="$EJECUTABLE fdisk"
#Si no tienes Zsh instalado en tu sistema (Mac incluye zsh por default)
#deberás instalar Zsh manualmente en ese caso.
#Cada comando fdisk va acompañado de un comentario que indica como debe ir
#quedando el disco. P significa primaria, F Free, E extendida,
#L logica, G libre dentro de una extendida.
#Este archivo tiene algunos errores lógicos. Los errores están acompañados de un comentario que indica porqué debería
#ocurrir el error. En la calificación se validarán algunos escenarios extra. Este archivo debe correr y ejecutarse sin 
#ningún problema, ya que cada comando es atómico. Si ocurre un error, simplemente se detiene la ejecución y no produce
#nada el comando que lanzó el error, pero el resto de comandos deben ejecutarse sin problema. Este archivo NO se ejecuta
#con exec. El archivo de entrada para exec está separado.

OFICIAL="/tmp/discos calificacion/disco oficial.disk"
T1=/tmp/mia/disco1.disk
T2=/tmp/mia/disco2.disk
T3=/tmp/mia/disco3.disk

mkdisk -path=$T1 -size=50 -u=m -f=WF # (50000 F)
mkdisk -path=$T3 -size=208 -u=m -f=FF # ( 208000 F)
mkdisk -f=FF -size=55 -u=k -path=$T2 # (55 F )
mkdisk -size=35 -u=m -f=BF -path="$OFICIAL" #(35000 F)
rmdisk -path=$T2
fdisk -path="$OFICIAL" -add=-35 -u=K -name=p1 #Error, p1 no existe.
fdisk -path="$OFICIAL" -size=3000 -u=K -type=P -name=p1 #(3000 P; 32000 F)
fdisk -size=2500 -path="$OFICIAL" -u=K -name=p2 #(3000P; 2500P; 29500 F)
fdisk -size=20000 -path="$OFICIAL" -f=BF -u=K -type=E -name=e1 #(3000P; 2500P; 20000E; 9500F)
fdisk -size=1 -path="$OFICIAL" -u=m -type=E -name=e3 #Error, solo 1 extendida en disco
fdisk -size=1 -path="$OFICIAL" -u=m -type=P -name=p3 #(3000P; 2500P; 20000E; 1000P; 8500F)
fdisk -delete=full -path="$OFICIAL" -name=p1 #(3000F; 2500P; 20000E; 1000P; 8500F)
fdisk -delete=full -path="$OFICIAL" -name=p2 #(5500F; 20000E; 1000P; 8500F)
fdisk -size=4 -path="$OFICIAL" -u=m -type=P -name=n1 #(4000P; 1500F; 20000E; 1000P; 8500F)
fdisk -add=1000 -path="$OFICIAL" -u=k -name=e1  #(4000P; 500F; 21000E; 1000P; 8500F) Debe dar espacio a la izquierda
fdisk -delete=fast -path="$OFICIAL" -name=p3  #(4000P; 500F; 21000E; 9500F)
fdisk -add=9000 -path="$OFICIAL" -u=k -name=e1  #(4000P; 500F; 30000E; 500F) 
  
fdisk -path="$T1" -size=3550 -u=K -type=P -name=p1 #(3550P; 46450F)
fdisk -size=10000 -path="$T1" -u=K -name=p2 #(3550P; 10000P; 36450)
fdisk -size=15 -path="$T1" -u=M -name=p3 -type=P #(3550P; 10000P; 15000P; 21450F)
fdisk -delete=fast -path="$T1" -name=p2 #(3550P; 10000F; 15000P; 21450F)
fdisk -size=9 -path="$T1" -u=M -f=FF -name=e1 -type=E #(3550P; 10000F; 15000P; 9000E; 12450F)
fdisk -add=-1500 -u=K -name=p1 -path="$T1" #(2050P; 11500F; 15000P; 9000E; 12450F)
fdisk -size=11 -u=M -path="$T1" -type=P -name="particion 4"  #(2050P; 11000P; 500F; 15000P; 9000E; 12450F)
fdisk -add=5000 -u=K -name="e1" -path="$T1"  #(2050P; 11000P; 500F; 15000P; 14000E; 7450F)
fdisk -size=1000 -u=k -path="$T1" -type=P -name="extra"  #Error, ya hay 4 particiones.
